/* 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Software PLL for HAM radio VFO up to 40 meter band <8Mhz
(c) 2020
Fernando Victor Filippetti  fvfilippetti@fi.uba.ar

TODO: Usar un comparador de fase tipo 3 (un solo F/F flanco positivo SR) para enganchar en fase
si se desengancha.
*/

#include <Arduino.h>
#include <avr/sfr_defs.h>

#define PC_OUT 9
#define REF_OUT 8
#define VCO_OUT 10
#define CHOP_Z 65000

void setup()
{
  Serial.begin(9600);
  pinMode(PC_OUT, OUTPUT);
  pinMode(REF_OUT, OUTPUT);
  pinMode(VCO_OUT, OUTPUT);
  //configuo el timer2 para tener una señal de referencia
  //set up timer with prescaler = 256
  //para el MEGA8:  TCCR2 |= (1 << CS22) | (1 << CS21);
  TCCR2B |= (1 << CS22) | (1 << CS21) | (1 << CS20) | (1 << WGM22); // clock/
  // enable overflow interrupt y CTC
  //para el MEGA8:TIMSK |= (1 << TOIE2);
  TIMSK2 |= (1 << TOIE2);
  //enable clear on compare match (CTC)
  TCCR2A |= (1 << WGM21) | (1 << WGM20);
  OCR2A = 78; //empiezo a contar

  //configuro el otro timer para la entrada de clock
  TCCR1B |= (1 << WGM12) | (1 << WGM13) | (1 << CS10) | (1 << CS12) | (1 << CS11); //modo CTC(fastpwm),sin prescaler
  TCCR1A |= (1 << WGM11) | (1 << WGM10);
  OCR1A = 10;
  TIMSK1 |= (1 << TOIE1);
  // enable global interrupts
  sei();
}

volatile uint8_t counts2, counts1, counts_frac, chop_n, fracc;
volatile uint16_t resto=16000;
volatile uint32_t freq = 1000;

void loop()
{
  char read_char;
   while (Serial.available() > 0) {
     read_char=Serial.read();
     if (read_char == '+') {
       resto+=10;
   }
   if (read_char == '-') {
       resto-=10;
   }
    Serial.println(resto);
   }
  // en estas variables seteo la fracuencia del PLL
  chop_n = 0;  //freq / (10 * CHOP_Z);
  //resto = 8000; //((freq / 10) % CHOP_Z);   // si pongo 2 divide por 3 (2+1)
  fracc = 0;   //(0-9) decimas de la frecuencia de comparación (de a 10 hz para F_ref=100);
}

ISR(TIMER1_OVF_vect)
{
  /* cuenta de a CHOP_Z, y programa para contar lo que le falta */
  if (counts1 == chop_n)
  {
    if (counts_frac == 10) //fractional part of N divider implementation
    {
      counts_frac = 0;
    }
    if (counts_frac > fracc)
    {
      OCR1A = resto;
    }
    else
    {
      OCR1A = resto + 1;
    }
    //cuento lo que me falta
    digitalWrite(VCO_OUT, 1 - digitalRead(VCO_OUT));
    counts1 = 0;
    counts_frac++;
  }
  else
  {
    OCR1A = CHOP_Z;
    counts1++;
  }
  digitalWrite(PC_OUT, digitalRead(VCO_OUT) ^ digitalRead(REF_OUT)); // 1/2 Comparador de fase XOR
}

ISR(TIMER2_OVF_vect)
{
  /* Toggle a pin on timer overflow */
  digitalWrite(REF_OUT, 1 - digitalRead(REF_OUT));
  OCR2A = 78;                // 16000000/1024/78 ~= 200 int por minuto -> 100 hz (78.125)
  if ((counts2 & 0x07) == 0) //cada 8 pulsos divido por M+1 para recuperar.
  {
    OCR2A = 79; //parte fraccional (0.125) (M+1)
    counts2++;
  }
  digitalWrite(PC_OUT, digitalRead(VCO_OUT) ^ digitalRead(REF_OUT)); // 1/2 Comparador de fase XOR
}
